var Resource = require('deployd/lib/resource')
  , Script = require('deployd/lib/script')
  , util = require('util')
  , when = require('when')
  , timeout = 2000;

function EventResource(name, context) {
  timeout = context.config.timeout || timeout;
  Resource.apply(this, arguments);
}
util.inherits(EventResource, Resource);

EventResource.label = "Event";
EventResource.events = ["get", "post"];
EventResource.basicDashboard = {
  settings: [{
    name        : 'timeout',
    type        : 'numeric',
    description : 'Timeout for all requests. Defaults to ' + timeout + '. Use -1 to disable timeout.'
  }]
};

module.exports = EventResource;

EventResource.prototype.clientGeneration = true;

EventResource.prototype.handle = function (ctx, next) {
  var parts = ctx.url.split('/').filter(function(p) { return p; });

  var to;
  var deferred = when.defer();
  if(timeout > 0) {
    deferred.promise.then(function() {
      clearTimeout(to);
    },function() {
      clearTimeout(to);
    });
    to = setTimeout(function() { deferred.reject('Timeout exceeded ' + timeout + 'ms'); }, timeout);
  }
  var result = {};

  var domain = {
      url: ctx.url
    , parts: parts
    , query: ctx.query
    , body: ctx.body
    , 'this': result
    , setResult: function(val) {
      result = val;
      deferred.resolve();
    }
  };

  if (ctx.method === "POST" && this.events.post) {
    this.events.post.run(ctx, domain, function(err) {
      if(err) deferred.reject(err);
      deferred.promise.finally(function() {
        ctx.done(err, result);
      });
    });
  } else if (ctx.method === "GET" && this.events.get) {
    this.events.get.run(ctx, domain, function(err) {
      if(err) deferred.reject(err);
      deferred.promise.finally(function() {
        ctx.done(err, result);
      });
    });
  } else {
    next();
  }

};
